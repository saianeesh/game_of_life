cmake_minimum_required (VERSION 3.24.3)

#### REMOVE BEFORE COMMITING ####
set (CMAKE_CXX_COMPILER g++)
# cmake -DCMAKE_CXX_COMPILER=g++


project (game_of_life VERSION 1.0)

set (CMAKE_CXX_STANDARD 14)
set (CMAKE_CXX_STANDARD_REQUIRED True)

set (SRC_DIR src)




# add libraries
#add_library(raylib STATIC IMPORTED)

#set_target_properties(raylib PROPERTIES
#    IMPORTED_LOCATION ${PROJECT_SOURCE_DIR}/libs
#    INTERFACE_INCLUDE_DIRECTORIES ${PROJECT_SOURCE_DIR}/include
#)



# add final deliverables
add_executable(game_of_life
    ${SRC_DIR}/game_of_life.cc
    ${SRC_DIR}/read_life_105.cc
    ${SRC_DIR}/read_rle.cc
    ${SRC_DIR}/board.cc
)


# directories to include (libraries and header files)
target_link_directories(game_of_life PUBLIC
    ${CMAKE_SOURCE_DIR}/libs
)

target_include_directories(game_of_life PUBLIC
    ${CMAKE_SOURCE_DIR}/include
)



# linking

if (MINGW)
    target_link_libraries(game_of_life PUBLIC
        raylib ${CMAKE_SOURCE_DIR}/libs/libraylib_win.a
        opengl32
        gdi32
        winmm
    )
endif (MINGW)

if (UNIX)
    target_link_libraries(game_of_life PUBLIC
        raylib ${CMAKE_SOURCE_DIR}/libs/libraylib.a
        GL
        pthread
        m
        X11
        dl
    )
endif (UNIX)
