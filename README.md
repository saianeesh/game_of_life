# Game Of Life
Written using C++14 and the [raylib](https://www.raylib.com) graphics library.

## Usage
### Options
- `-n <num>` allows setting the number of cells (the grid is always a square, so the closest square is chosen)
- `-i <input_file>` reads a .rle/.lif (Life 1.05) file and places the pattern at (0, 0)
- `-x <x_offset>` offsets the pattern (from the top-left cell) read by `x_offset` amount on the x-axis 
- `-y <y_offset>` offsets the pattern (from the top-left cell) read by `y_offset` amount on the y-axis 
*By default places the pattern in the approximate middle of the board*
### Controls
- `<space>` toggles the play/pause status of the game

## Building
Uses the [raylib](https://github.com/raysan5/raylib/wiki/Working-on-GNU-Linux) library (click the link to learn how to build and link it).
```
make
```
*Currently only tested on GNU/Linux*
