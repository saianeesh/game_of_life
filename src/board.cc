#include <raylib.h>
#include <math.h>

#include "game_of_life.hh"
#include "board.hh"


Board::Board(unsigned int cell_count)
{
    screen.width = screen.height = 640;

    rows = cols = (int) sqrt(cell_count);
    cellWidth = cellHeight = screen.width / (float) cols;

    cells = new Cell[rows * cols];
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            Cell *curr =  cells + ((cols * row) + col); //cells[(cols * row) + col]
            curr->alive = false;
            curr->x = (col * cellWidth);
            curr->y = (row * cellHeight);
        }
    }
}

Board::~Board(void)
{
    delete[] cells;
}

Board&
Board::operator=(const Board& p)
{
    if (this != &p) {
        if (rows * cols != p.rows * p.cols) {
            delete[] cells;
            cells = new Cell[rows * cols];
        }
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                Cell *cell = get_cell(row, col);
                const Cell *pcell = p.get_cell(row, col);

                cell->alive = pcell->alive;
                cell->x = pcell->x;
                cell->y = pcell->y;
            }
        }
        rows = p.rows;
        cols = p.cols;
        cellHeight = p.cellHeight;
        cellWidth = p.cellWidth;
        screen = p.screen;
    }
    return *this;
}

float
Board::get_screen_height(void) const
{
    return screen.height;
}

float
Board::get_screen_width(void) const
{
    return screen.width;
}

void
Board::resize()
{
    double widthRatio = (GetScreenWidth() / screen.width);
    double heightRatio = (GetScreenHeight() / screen.height);
    cellHeight *= heightRatio;
    cellWidth *= widthRatio;
    screen.width = GetScreenWidth();
    screen.height = GetScreenHeight();
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            Cell *curr = get_cell(row, col);
            curr->x = (col * cellWidth);
            curr->y = (row * cellHeight);
        }
    }
}

double
Board::get_cellheight(void)
{
    return cellHeight;
}

double
Board::get_cellwidth(void)
{
    return cellWidth;
}

void
Board::select(Vector2 coord)
{
    int ncol = coord.x / cellWidth;
    int nrow = coord.y / cellHeight;
    cells[(cols * nrow) + ncol].alive ^= true;
}

Cell *
Board::get_cell(int row, int col) const
{
    if (row >= rows || row < 0 || col >= cols || col < 0) {
        return 0;
    }
    return cells + ((cols * row) + col);
}

int
Board::get_weighting(int row, int col)
{
    Cell *cell = get_cell(row, col);
    int select[] = { -1, 0, 1 };
    int weight = 0;
    for (int srow = 0; srow < 3; srow++) {
        for (int scol = 0; scol < 3; scol++) {
            Cell *curr = get_cell(row + select[srow], col + select[scol]);
            if (curr == cell) continue;
            if (curr && curr->alive) weight++;
        }
    }
    return weight;
}


void
Board::next(void)
{
    /* get weights of all cells */
    int *weights = new int[rows * cols];
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            weights[(cols * row) + col] = get_weighting(row, col);
        }
    }
    /* apply rules to all cells */
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            int weight = weights[(cols * row) + col];
            Cell * const curr = get_cell(row, col);
            if (curr->alive) {
                // alive
                if (weight != 2 && weight != 3) {
                    curr->alive = false;
                }
            } else if (!curr->alive) {
                // dead
                if (weight == 3) {
                    curr->alive = true;
                }
            }
        }
    }
    delete[] weights;
}

void
Board::reset(void)
{
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            Cell *curr =  get_cell(row, col); //cells[(cols * row) + col]
            curr->alive = false;
        }
    }
}
