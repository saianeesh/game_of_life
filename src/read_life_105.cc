
#include <iostream>
#include <fstream>
#include <ctype.h>
#include <string>

#include "read_life_105.hh"
#include "game_of_life.hh"
#include "board.hh"


using namespace std;

namespace life105 {
    namespace {
        const char dead = '.';
        const char alive = '*';
        const char comment = '#';
    }
    unsigned int find_rows(string);
    unsigned int find_cols(string);
}

unsigned int
life105::find_rows(string infile)
{
    unsigned int prows = 0;
    ifstream row_file(infile);
    for (string tmp; getline(row_file, tmp); ) {
        if (tmp[0] == comment || isspace(tmp[0])) {
            continue;
        }
        prows++;
    }
    row_file.close();
    return prows;
}

unsigned int
life105::find_cols(string infile) 
{
    unsigned int pcols = 0;
    ifstream col_file(infile);
    for (string tmp; getline(col_file, tmp); ) {
        if (tmp[0] == comment || isspace(tmp[0])) {
            continue;
        }
        pcols = tmp.length();
        break;
    }
    col_file.close();
    return pcols;
}

bool
life105::read(string infile, Board &b, unsigned int off_x, unsigned int off_y)
{
    ifstream file(infile);
    unsigned int row = 0;
    unsigned int prows = find_rows(infile), pcols = find_cols(infile);
    unsigned int poff_x = ((int) b.get_cols() / 2) - ((int) pcols / 2);
    unsigned int poff_y = ((int) b.get_rows() / 2) - ((int) prows / 2);
    for (string line; getline(file, line); ) {
        if (line[0] == comment || isspace(line[0])) {
            continue;
        }
        // pattern lines
        for (unsigned int col = 0; col < line.length(); col++) {
            Cell *curr = b.get_cell(poff_y + row, poff_x + col);
            if (curr == 0) {
                break;
            }
            curr->alive = (line[col] == alive) ? true : false;
        }
        row++;
    }
    file.close();
    return true;
}
