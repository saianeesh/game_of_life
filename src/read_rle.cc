#include <fstream>
#include <iostream>
#include <ctype.h>
#include <string>

#include "read_rle.hh"
#include "board.hh"
#include "game_of_life.hh"

using namespace std;

namespace rle {
    namespace {
        const char comment = '#';
        const char dead    = 'b';
        const char alive   = 'o';
        const char eol     = '$';
        const char eof     = '!';
        unsigned int px = 0, py = 0;
    }

    bool read_line(string, Board &, unsigned int, unsigned int, unsigned int);
}


/* can exit program */
bool
rle::read_line(string line, Board &b, unsigned int prow, unsigned int off_x, unsigned int off_y)
{
    unsigned int pcol = 0;
    for (size_t i = 0; i < line.length(); i++) {
        char c = line[i];
        int rep = 1;
        if (isdigit(c)) {
            size_t tmp_i;
            rep = stoi(line.substr(i), &tmp_i); // increments i
            i += tmp_i;
            c = line[i];
        }
        for (; rep > 0 ; rep--, pcol++) {
            Cell *curr = b.get_cell(off_y + prow, off_x + pcol);
            if (curr ==  0) {
                cerr << "Offsets are outside the defined borders, increase n!" << endl;
                exit(EXIT_FAILURE);
            }
            curr->alive = (c == alive ? true : false);
        }
    }

    return true;
}

void
rle::read(string infile, Board &b, int off_x, int off_y) // off_x = 0, off_y = 0 by default
{
    ifstream file(infile, ios_base::in); // open 'infile' in ios_base::in (reading) mode

    for (string line; getline(file, line); ) {
        if (line[0] == comment) { // comment
            continue;
        }
        if (line[0] == 'x') { // header
            string x_str, y_str;
            try {
                x_str = line.substr(line.find("=") + 1);
                y_str = x_str.substr(x_str.find("=") + 1);

                x_str.erase(x_str.find(","));
                if (line.find("rule") != string::npos) {
                    y_str.erase(y_str.find(","));
                } 

            } catch (const out_of_range& oor) {
                cerr << "Wrong header format!" << endl;
                return;
            }
            px = stoi(x_str);
            py = stoi(y_str);
            break;
        }
    }
    
    /* center of the board as offset */
    unsigned int poff_x = (off_x < 0) ? ((int) b.get_cols() / 2) - ((int) px / 2) : off_x;
    unsigned int poff_y = (off_y < 0) ? ((int) b.get_rows() / 2) - ((int) py / 2) : off_y;
    unsigned int prow = 0;
    for (string line; getline(file, line, eol) && prow < py; read_line(line, b, prow, poff_x, poff_y), prow++);

    file.close();
}
