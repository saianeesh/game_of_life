#include <stdlib.h>
#include <iostream>
#include <unistd.h>

#include <raylib.h>
#include "game_of_life.hh"
#include "board.hh"
#include "read_rle.hh"
#include "read_life_105.hh"


enum {
    RLE,
    LIFE_105,
    UNSUPPORTED
};

int
filetype(std::string filename)
{
    using namespace std;
    string ext = filename.substr(filename.find('.') + 1);
    string life = "lif", rle = "rle";
    if (ext.compare(life) == 0 || ext.compare(rle) == 0) {
        if (ext.compare(life) == 0) {
            /* life 1.05 */
            return LIFE_105;
        } else if (ext.compare(rle) == 0) {
            /* rle */
            return RLE;
        }
    }

    return UNSUPPORTED;
}

void
help(void)
{
    using namespace std;
    cout << "usage: game_of_life [-n <num>] [-i <input_file>] [-x <x_offset>] [-y <y_offset>]" << endl;
    cout << "\t-n <num>\tthe number of cells to use (closed square is selected)" << endl;
    cout << "\t-i <input_file>\tthe input pattern file .rle or .lif (Life 1.05)" << endl;
    cout << "\t-x <x_offset>\tthe amount of columns to offset the pattern by, from the top-left cell of the pattern" << endl;
    cout << "\t-y <y_offset>\tthe amount of rows to offset the pattern by, from the top-left cell of the pattern" << endl;
    exit(EXIT_SUCCESS);
}

void
draw(Board& board)
{
    for (int row = 0; row < board.rows; row++) {
        for (int col = 0; col < board.cols; col++) {
            const Cell curr = board.cells[(row * board.cols) + col];
            Color color = (curr.alive ? YELLOW : DARKGRAY);
            Rectangle rec = {curr.x, curr.y, board.cellWidth, board.cellHeight};
            DrawRectangleRec(rec, color);
        }
    }

    /* draw square borders */
    Vector2 start, end;
    /* rows */
    for (int row = 0; row < board.rows; row++) {
        DrawLineV(start = {0, row * board.cellHeight}, end = {board.screen.width, row * board.cellHeight}, BLACK);
    }
    /* cols */
    for (int col = 0; col < board.cols; col++) {
        DrawLineV(start = {col * board.cellWidth, 0}, end = {col * board.cellWidth, board.screen.height}, BLACK);
    }
}

/*
 * :param board -> is an object of the Board class
 */
void
input(Board& board)
{
    static bool play = false;
    if (!play && IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
        Vector2 coord = GetMousePosition();
        board.select(coord);
    } else if (IsKeyPressed(KEY_SPACE)) {
        play ^= true;
    }

    if (play) {
        board.next();
    }
}


/*
 * cmd options:
 *     -n1 | -n 1 : the number of cells
 *     -ifilename | -i filename : the input file
 *     -ofilename | -o filename : the output file
 */
int
main(int argc, char *argv[]) 
{
    // optind : index of the next element to be processed
    int opt = 0, num_cells = 1000;
    int offx = -1, offy = -1;
    std::string infile, outfile, num_str;
    size_t stoi_err;
    while ((opt = getopt(argc, argv, ":n:i:o:hx:y:")) != -1) {
        switch (opt) {
            case 'n':
                num_str = optarg;
                num_cells = std::stoi(num_str, &stoi_err);
                if (stoi_err == 0) {
                    std::cerr << "The -n option requires a number argument" << std::endl;
                }
                break;
            case 'x':
                offx = std::stoi(optarg, &stoi_err);
                if (stoi_err == 0 || offx < 0) {
                    std::cerr << "The -x option requires a positive integer" << std::endl;
                }
                break;
            case 'y':
                offy = std::stoi(optarg, &stoi_err);
                if (stoi_err == 0 || offy < 0) {
                    std::cerr << "The -y option requires a positive integer" << std::endl;
                }
                break;
            case 'i':
                infile = optarg;
                break;
            case 'o':
                outfile = optarg;
                break;
            case 'h':
                help();
                break;
            case '?':
                std::cerr << "Invalid option!" << std::endl;
                break;
            case ':':
                std::cerr << (char) opt << " is missing a mandatory argument!" << std::endl;
                break;
            default:
                ;
        }
    }
    SetConfigFlags(FLAG_WINDOW_RESIZABLE); // able to resize the window
                                           //
    Board board(num_cells); 

    if (!infile.empty()) {
        int type = filetype(infile);
        switch (type) {
            case RLE:
                rle::read(infile, board, offx, offy);
                break;
            case LIFE_105:
                life105::read(infile, board, offx, offy);
                break;
            default:
                // i.e. when UNSUPPORTED (or other) are returned
                help();
        }
    }

    InitWindow(board.get_screen_width(), board.get_screen_height(), "Game Of Life");
    if (!IsWindowReady()) {
        std::cerr << "Window not ready" << std::endl;
        exit(EXIT_FAILURE);
    }
    SetTargetFPS(60);

    while (!WindowShouldClose()) {
        if (IsWindowResized()) {
            board.resize();
        }
        BeginDrawing();
            draw(board);
        EndDrawing();

        input(board);
    }

    CloseWindow();
    return 0;
}
