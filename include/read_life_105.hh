#ifndef _READ_LIFE_105_
#define _READ_LIFE_105_

#include "board.hh"

namespace life105 {
    bool read(std::string, Board &, unsigned int = 0, unsigned int = 0);
}

#endif

