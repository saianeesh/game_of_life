#ifndef _READ_RLE_
#define _READ_RLE_

#include "board.hh"
#include <string>

namespace rle {
    void read(std::string, Board &, int off_x = -1, int off_y = -1);
}

#endif
