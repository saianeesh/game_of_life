#ifndef __BOARD_H_
#define __BOARD_H_

#include <string>

#include <raylib.h>
#include "game_of_life.hh"


class Board {
    private:
        //void draw_cell(Cell *) const;
        int rows, cols;
        float cellHeight, cellWidth;

        struct {
            float width;
            float height;
        } screen; 

    public:
        Cell *cells; // array of cells

        Board(unsigned int = 1000);
        ~Board(void);
        Board& operator=(const Board&);

        Cell *get_cell(int, int) const;
        void next(void);
        int get_weighting(int, int);
        void select(Vector2);
        double get_cellheight(void);
        double get_cellwidth(void);
        void resize(void);
        float get_screen_height(void) const;
        float get_screen_width(void) const;
        void reset(void);

        int get_rows(void) { return rows; }
        int get_cols(void) { return cols; }

        friend void draw(Board& board);

        /* experiment */
        //void on_every_cell(void (*)(Cell *));
};

#endif
